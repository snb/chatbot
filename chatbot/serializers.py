from rest_framework import serializers

from chatbot.models import QuestionList


class QuestionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionList
        fields = ('id', 'name')
